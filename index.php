<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php


//PARTIE 1: Exercice 1

$variable;
for($variable=0; $variable<=10; $variable++)
{
    if($variable<10)
    {
        echo '<p>Résultat exercice 1 : ', $variable, '</p>';
    }
}

//Exercice 2

$variable1=0;
$variable2=20;

while($variable1<= 20)
{

    $resultat= $variable1*$variable2;
    echo '<p>Résultat exercice 2 : ', $resultat, '</p>';
    $variable1 ++;

}


//Exercice 3

$var1 =100;
$var2= 53;

while ($var1 >= 10)
{
    $resultat=$var1*$var2;
    echo '<p>Résultat exercice 3 : ', $resultat ,'</p>';
    $var1 --;
}


// Exercice 4

$maVariable=1;
while($maVariable<=10)
{
    echo '<p>Résultat exercice 4 : ', $maVariable ,'</p>';
    $maVariable +=$maVariable/2;
}

//Exercice 5

$i=0;
while($i<= 15)
{
    $i++;
    echo '<p>Résultat exercice 5 : ', $i, "On y arrive presque..." ,'</p>';
}


//Exercice 6
$a=20;
while($a>0)
{
    $a--;
    echo '<p>Résultat exercice 6 : ', $a."On tient le bon bout..." ,'</p>';
}


//Exercice 7

$coucou=1;
while($coucou<=100)
{
    $coucou+=15;
    echo '<p>Résultat exercice 7 : ', $coucou."C'est presque bon..." ,'</p>';
}


//Exercice 8

$hello=200;
while($hello>0)
{
    $hello-=12;
    echo '<p>Résultat exercice 8 : ', $hello."Enfin ! ! !" ,'</p>';

}
?>

<!-- PARTIE 2 -->
<!-- Les paramètres


Les formulaires

Exercice 1 **Créer un formulaire demandant le nom et le prénom. 
Ce formulaire doit rediriger vers la page user.php avec la méthode GET.  -->

<form action="user.php" method="get">
	<label>Nom</label> : <input type="text" name="nom" />
	<label>Prénom</label> : <input type="text" name="prenom" />
	<input type="submit" value="Envoyer" />
</form>


<!-- Exercice 2 Créer un formulaire demandant le nom et le prénom. 
Ce formulaire doit rediriger vers la page user.php avec la méthode POST. -->
<form action="user.php" method="post">
	<label>Nom</label> : <input type="text" name="tonnom" />
	<label>Prénom</label> : <input type="text" name="tonprenom" />
	<input type="submit" value="Envoyer" />
</form>


<!-- Exercice 5 -->

<form action="user.php" method="post">
    <select name="votrecivilite" size="1">
    <OPTION>Mr
    <OPTION>Mme
    <OPTION>Mlle
    </select>
    <label>Nom</label> : <input type="text" name="votrenom" />
	<label>Prénom</label> : <input type="text" name="votreprenom" />
	<input type="submit" value="Envoyer" />


</form>


</body>
</html>
