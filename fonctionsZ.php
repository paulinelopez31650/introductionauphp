<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
</head>
<body>
    
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Exercice  1 & 2 : ne pas faire

// Exercice 3: Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.


function maVariable($hello1="popo", $hello2="lopez")
{
    return $hello1 . " ". $hello2;
}

echo maVariable();

// Exercice 4 :Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

//     "Le premier nombre est plus grand" : si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux


function mesNombres($nombreA=70, $nombreB=90)
{
        if($nombreA>$nombreB)
        {
            return "Le premier nombre est plus grand";
        }
        if($nombreA<$nombreB)
        {
            return "Le premier nombre est plus petit";
        }
        if($nombreA==$nombreB)
        {
            return "Les deux nombres sont identiques";
        }
}

echo mesNombres();

// Exercice 5: Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.


function lesDeux($unNombre=43, $uneChaine="mamamiaaaaaaa")
{
    return $unNombre." ".$uneChaine;
}

echo lesDeux();

// Exercice 6: Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".

function autreExercice($nom="LOPEZ", $prenom="Pauline", $age=28)
{

return "bonjour" . $nom . $prenom . "tu as " . $age ."ans";

}

echo autreExercice();

// Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. 
// Le paramètre genre peut prendre comme valeur Homme ou Femme. 
// La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur


function sexe($genre, $age)
{
    if($age >=18 && $genre=="femme")
    {
        echo "vous etes une femme et vous etes majeur";
    }
    elseif($age <=18 && $genre=="femme")
    {
        echo "vous etes une femme et vous etes mineur";
    }
    elseif($age <=18 && $genre=="homme")
    {
        echo "vous etes un homme et vous etes mineur";
    }
    elseif($age >=18 && $genre=="homme")
    {
        echo "vous etes un homme et vous etes majeur";
    }
}

sexe("femme", 12);

// Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. 
// Tous les paramètres doivent avoir une valeur par défaut.



function troisNombres($one, $two, $three)
{
    $addition= $one + $two + $three;
    echo $addition;
}

troisNombres(1,2, null);

//Les tableaux

function afficher($a)
{
    echo '<p>'.$a."</p>";
}

$mois = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Decembre",
];

//Exercice 2

afficher($mois[2]);

//Exercice 3

afficher($mois[5]);

//Exercice 4 : modifier le mois de aout pour lui ajouter l'accent manquant.

$mois[7]="Août";

//Exercice 5: 

$encoreDesMois = [
    1 => "Janvier",
    2 =>"Février",
    3 =>"Mars",
    4 =>"Avril",
    5 =>"Mai",
    6 =>"Juin",
    7 =>"Juillet",
    8 =>"Aout",
    9 =>"Septembre",
    10 =>"Octobre",
    11 =>"Novembre",
    12 => "Decembre",
];

//Exercice 7: ajouter un élement du tableau

$encoreDesMois[13]="nouveauMois";

// Exercice 8 : Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.

foreach($mois as $i =>$v)
{
    afficher("le mois de ".$v."est le numéro".$i);
}

// AVEC LA BOUCLE FOR:
// for($i=1;$i<=count($encoreDesMois); $i++)
// {
//     afficher($encoreDesMois[$i]);
// }
?>


</body>
</html>